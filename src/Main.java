import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
public class Main {

public static void main(String[] args)  throws IOException {


                CSVParser cvsParser = new CSVParser();

                List<String> lines = Files.readAllLines(Paths.get("movementList.csv"));
                for (int i = 1; i < lines.size(); i++) {
                    cvsParser.calculateData(lines.get(i));
                }

                cvsParser.printData();
            }
        }